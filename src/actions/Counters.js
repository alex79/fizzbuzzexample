export const CounterActions = {
  USE_COUNTER: 'USE_COUNTER',
  INCREMENT_COUNTER: 'INCREMENT_COUNTER',
  DECREMENT_COUNTER: 'DECREMENT_COUNTER',
};

export const useCounter = counter => ({
  type: CounterActions.USE_COUNTER,
  counter,
});

export const incrementCounter = () => ({
  type: CounterActions.INCREMENT_COUNTER,
});

export const decrementCounter = () => ({
  type: CounterActions.DECREMENT_COUNTER,
});