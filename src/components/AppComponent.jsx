import React from 'react';

export class AppComponent extends React.PureComponent {
  _divisibleBy(number) {
    const { currentNumber } = this.props;

    return ( currentNumber % number === 0 );
  }

  _fizzBuzzText() {
    const divisibleBy3 = this._divisibleBy(3);
    const divisibleBy5 = this._divisibleBy(5);

    if ( divisibleBy3 && divisibleBy5 ) {
      return 'FizzBuzz';
    } else if ( divisibleBy3 ) {
      return 'Fizz';
    } else if ( divisibleBy5 ) {
      return 'Buzz';
    }

    return 'Neither';
  }

  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="col s3">
            <h1>Fizz Buzz</h1>
          </div>
        
          <div className="col s6">
            <div className="card">
              <div className="card-content">
                <article className="center-align">
                  <h3>Current Number: {this.props.currentNumber}</h3>

                  <h5>Fizz, Buzz, or FizzBuzz?<br/>{this._fizzBuzzText()}</h5>
                </article>
              </div>

              <div className="card-action grey lighten-4">
                <div className="row">
                  <div className="col s6 left-align">
                    <button className="waves-effect waves-light btn red lighten-1" onClick={this.props.decrementCounter}>
                      <i className="material-icons left">remove</i>
                      Decrement
                    </button>
                  </div>

                  <div className="col s6 right-align">
                    <button className="btn" onClick={this.props.incrementCounter}>
                      <i className="material-icons left">add</i>
                      Increment
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
