import { connect } from 'react-redux';
import { AppComponent } from '../components/AppComponent';
import { getCurrentNumber } from '../selectors/Counters';
import { useCounter, incrementCounter, decrementCounter } from '../actions/Counters'; 

const mapStateToProps = state => {
  return {
    currentNumber: getCurrentNumber(state),
  };
};

const mapDispatchToProps = dispatch => {
  const counterName = 'APP_COUNTER';

  return {
    incrementCounter: () => {
      dispatch(useCounter(counterName))
      dispatch(incrementCounter());
    },
    decrementCounter: () => {
      dispatch(useCounter(counterName))
      dispatch(decrementCounter());
    }
  };
};

export const App = connect(mapStateToProps, mapDispatchToProps)(AppComponent);