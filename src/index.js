import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux'
import 'materialize-css';
import './css/index.css';
import {App} from './containers/App';
import registerServiceWorker from './registerServiceWorker';
import configureStore from './store';

ReactDOM.render(
  <Provider store={configureStore()}>
    <App />
  </Provider>
, document.getElementById('root'));
registerServiceWorker();
