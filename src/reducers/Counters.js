import { CounterActions } from '../actions/Counters';

export default (state = {
  items: { 
    'default': 1,
  },
  currentCounter: 'default',
}, action) => {
  switch (action.type) {
    case CounterActions.USE_COUNTER:{
      const counter = state.items[action.counter] || 1;

      return {
        ...state,
        items: {
          ...state.items,
          [action.counter]: counter,
        },
        currentCounter: action.counter,
      }
    }
    case CounterActions.INCREMENT_COUNTER:
      return {
        ...state,
        items: {
          ...state.items,
          [state.currentCounter]: state.items[state.currentCounter] + 1,
        },
      }
    case CounterActions.DECREMENT_COUNTER:
      return {
        ...state,
        items: {
          ...state.items,
          [state.currentCounter]: Math.max(1, state.items[state.currentCounter] - 1),
        },
      }
    default:
      return {
        ...state,
      }  
  }
}