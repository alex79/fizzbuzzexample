import { combineReducers } from 'redux';

import counters from './Counters';

export default combineReducers({
 counters
});