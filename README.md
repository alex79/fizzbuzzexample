Fizz Buzz Sample based on Create React App

### Dependencies used:
1. materialize-css - SASS library that's compatible with Material Design  
2. redux - State management library, necessary for managing n-counters across the app    

### Dev Dependencies used:
1. node-sass-chokidar - Used to compile SASS files  
2. concurrently - Used to run react-scripts and node-sass-chokidar at the same time